<?php

namespace Uncgits\GrouperApiLaravel;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // register artisan commands, other CLI stuff
        if ($this->app->runningInConsole()) {
            // publish config
            $this->publishes([
                __DIR__ . '/publish/config' => base_path('config'),
            ], 'config');
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $bindType = config('grouper-api.use_singleton', true) ? 'singleton' : 'bind';

        $this->app->$bindType('Grouper', function ($app) {
            return new GrouperApiInterface();
        });
    }
}
