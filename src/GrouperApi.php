<?php

namespace Uncgits\GrouperApiLaravel;

use Uncgits\GrouperApiLaravel\Events\ApiCallFinished;
use Uncgits\GrouperApiLaravel\Events\ApiCallStarted;
use Illuminate\Support\Facades\Cache;
use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Support\Facades\Log;

use Uncgits\GrouperApi\GrouperApi as BaseApiModel;

class GrouperApi extends BaseApiModel
{
    protected $historyDays;

    protected $cacheActive;
    protected $endpointsToCache;
    protected $cacheMinutes;

    protected $debugMode;

    public function __construct()
    {

        // set vars specific to this instance of Laravel
        $this->setHost(config('grouper-api.host'));
        $this->setUsername(config('grouper-api.username'));
        $this->setPassword(config('grouper-api.password'));
        $this->setApplicationName(config('app.name'));

        if (config('grouper-api.http_proxy.enabled') == 'true') {
            $this->setUseProxy(true);
            $this->setProxyHost(config('grouper-api.http_proxy.host'));
            $this->setProxyPort(config('grouper-api.http_proxy.port'));
        }

        // history days configuration
        $this->historyDays = config('grouper-api.history_days');

        // cache configuration
        $this->cacheActive = config('grouper-api.cache_active');
        $this->methodsToCache = config('grouper-api.cache_methods');
        $this->cacheMinutes = config('grouper-api.cache_minutes');

        // debug mode (not enabled in production)
        $this->debugMode = (config('app.env') == 'production') ? false : config('grouper-api.debug_mode');
    }

    /**
     * apiCall() - performs a call to the Grouper JSON REST API
     * https://my.grouper.endpoint/{{service}}/{{stem:with:possible:subordinates}}:{{group}}/{{operation}}
     *
     * @param string $method - HTTP request method: GET, POST, PATCH, DELETE, etc.
     * @param string $service - service category (first parameter)
     * @param $stem - complete stem including subordinates (include all colons except the last one)
     * @param $group
     * @param $operation
     *
     * @return array
     * @throws \Exception
     */

    protected function apiCall($method, $service, $stem = null, $group = null, $operation = null, $body = null)
    {
        $callingMethod = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function'];

        return parent::apiCall($method, $service, $stem, $group, $operation, $body);
        // skip this stuff for now.

        $method = strtolower($method);

        if ($method == 'get') {
            $endpoint = $service . '.' . $operation;
            $hashedCacheKey = hash('sha256', $service . $stem. $group . $operation);
        } elseif ($method == 'post') {
            $endpoint = $service;
        }

        $this->debugMessage('Hashed cache key: ' . $hashedCacheKey);

        if ($this->cacheActive && Cache::has($hashedCacheKey)) {
            // in cache
            $resultArray = Cache::get($hashedCacheKey);
            $returnArray = [
                'source'  => 'cache',
                'created' => Cache::get($hashedCacheKey . '-created', 'unknown')
            ];
            $message = 'API data for ' . $endpoint . ' retrieved from cache (created: ' . $returnArray['created'] . ')';
            $messageLevel = 'success';
        } else {
            $callMeta = [
                'service'   => $service,
                'stem'      => $stem,
                'group'     => $group,
                'operation' => $operation
            ];

            // not in cache or cache inactive
            $this->debugStartMeasure('apiCall', 'Making API call to Grouper');
            event(new ApiCallStarted($callMeta));
            $resultArray = parent::xmlApiCall($method, $service, $stem, $group, $operation, $body);
            event(new ApiCallFinished($callMeta));
            $this->debugStopMeasure('apiCall');

            $created = time();

            // cache if the call was successful, and we are supposed to cache it...
            if ($resultArray['response']['httpCode'] == 200) {
                $messageLevel = 'success';
                $message = 'API call to ' . $endpoint . ' successful.';

                if ($this->cacheActive) {
                    if (in_array($callingMethod, $this->methodsToCache[$method])) {
                        Cache::put($hashedCacheKey, $resultArray, now()->addMinutes($this->cacheMinutes));
                        Cache::put($hashedCacheKey . '-created', $created, now()->addMinutes($this->cacheMinutes));
                        $message .= ' Result cached in API cache.';
                    } else {
                        $message .= ' Not a cacheable API call.';
                    }
                }
            } else {
                $message = 'API error calling ' . $endpoint . ': ' . $resultArray['response']['httpCode'] . ' - ' . $resultArray['response']['httpReason'] . ' (' . $resultArray['response']['message'] . ')';
                $messageLevel = 'danger';
            }
            $returnArray = [
                'source'  => 'api',
                'created' => $created
            ];
        }

        // handle notifications / logging / etc.
        $notificationMode = config('grouper-api.notification_mode');

        if (in_array('flash', $notificationMode)) {
            flash($message, $messageLevel);
        }

        if (in_array('log', $notificationMode)) {
            if ($messageLevel == 'danger') {
                Log::error($message);
            } else {
                Log::info($message);
            }
        }

        $finalArray = array_merge($returnArray, $resultArray);

        $this->debugInfo($finalArray);

        return array_merge($finalArray);
    }

    // debug helpers

    protected function debugMessage($message, $label = null)
    {
        if ($this->debugMode) {
            if (!is_null($label)) {
                Debugbar::addMessage($message, $label);
            } else {
                Debugbar::addMessage($message);
            }
        }
    }

    protected function debugInfo($object)
    {
        if ($this->debugMode) {
            Debugbar::info($object);
        }
    }

    protected function debugStartMeasure($key, $label = '')
    {
        if ($this->debugMode) {
            Debugbar::startMeasure($key, $label);
        }
    }

    protected function debugStopMeasure($key)
    {
        if ($this->debugMode) {
            Debugbar::stopMeasure($key);
        }
    }
}
