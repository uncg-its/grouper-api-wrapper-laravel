<?php

namespace Uncgits\GrouperApiLaravel\Exceptions;

use Exception;

class GrouperApiException extends Exception
{
    public $response;
    public $more;

    public function __construct($response, $more = '') {
        $this->response = $response;
        $this->more = $more;
    }
}
