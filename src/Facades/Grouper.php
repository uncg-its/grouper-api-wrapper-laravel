<?php

namespace Uncgits\GrouperApiLaravel\Facades;

use Illuminate\Support\Facades\Facade;

class Grouper extends Facade
{

    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Grouper'; // the IoC binding.
    }
}
