<?php

namespace Uncgits\GrouperApiLaravel;

use Uncgits\GrouperApi\GrouperApiInterface as BaseApiInterface;

/**
 * This class will interface with the Grouper API to accomplish tasks. This "middle man" class will provide semantic, task-driven,
 * and logical methods and will do two main things:
 *
 * 1. Understand how to call the Grouper API to complete the request / operation / transaction, and
 * 2. Understand how to parse and communicate the results back to the user in the way they expect.
 *
 */
class GrouperApiInterface extends BaseApiInterface
{
    public function __construct()
    {
        parent::__construct();
        // override parent API object with ours.
        $this->api = new GrouperApi;

        // set defaults
        $this->setPageSize(config('grouper-api.results_per_page'));
        $this->setSubjectAttributeNames(config('grouper-api.subject_attribute_names'));

        $this->setPersonIdentifiers(config('grouper-api.person_identifiers'));
        $this->setGroupIdentifiers(config('grouper-api.group_identifiers'));
    }
}
