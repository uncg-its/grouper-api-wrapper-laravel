# Grouper API Library - Laravel Wrapper

Contact: matt.libera@uncg.edu

# Introduction

This package is a Laravel wrapper for the UNCG Grouper API PHP Library package, so that the Grouper API PHP Library can be used in Laravel apps.

> This is a **work in progress**. Not recommend for production apps just yet.

---

# Installation

1. `composer require 'uncgits/grouper-api-wrapper-laravel'`
2. *IF running Laravel 5.4 or below*: Add `Uncgits\GrouperApiLaravel\ServiceProvider::class,` to your `config/app.php` file
3. Run `php artisan vendor:publish --provider='Uncgits\GrouperApiLaravel\ServiceProvider'` - to publish the `grouper-api.php` config file

## Dependencies

This package has dependencies on `uncgits/grouper-api-php-library` and `barryvdh/laravel-debugbar` (dev)

---

# Configuration

Set your environment credentials in your `.env` file, and set your configuration options in `config/grouper-api.php`

## Options

The `grouper-api.php` configuration file contains some Laravel-specific settings and preferences that you can adjust as you see fit for your application. There is more detailed information on all of these below in the "Usage" section.

```php
// the 'Grouper' key is bound to the container (pointing to the 'GrouperApiInterface' class). this will control whether it is bound using bind() or singleton()
'use_singleton' => true,

// controls how you receive notifications from the application around API call success and cache
'notification_mode' => [], // flash, log, or empty array for none

// provides extra debug info in the debugbar package. should always be set to false in production environments.
'debug_mode' => false, // true - extra logging in Barryvdh Debugbar
```

## Grouper Credentials and URLs

> Note: do not use protocols (http:// or https://) for `GROUPER_API_HOST` - it should just be the hostname to your base API endpoint (e.g. my.grouper.url, NOT my.grouper.url/grouper-ws/servicesRest/v1_3_000)

```
GROUPER_API_HOST=
GROUPER_API_USERNAME=
GROUPER_API_PASSWORD=
```


## API Settings

To control how many results appear on each "page" of results from the API, set the `GROUPER_API_RESULTS_PER_PAGE` variable in your `.env` file. (Default is 25)

You should also configure the following `.env` variables, according to how your Grouper instance operates:

```
# All take comma-separated lists.

GROUPER_API_SUBJECT_ATTRIBUTE_NAMES= # default attribute names that will be used with withSubjectAttributeNames() and withSubjectAttributeNamesString()
GROUPER_API_PERSON_IDENTIFIERS= # source id strings that identify a person
GROUPER_API_GROUP_IDENTIFIERS= # source id strings that identify a group
```

## Cache settings

As of right now, the cache is not supported. Please experiment with these at your own risk.

---

# Usage

## Basic Usage / Getting Started

In your code, assuming you've set your information/credentials properly in your `.env` file, you should be able to instantiate the `Uncgits\GrouperApiLaravel\GrouperApiInterface` class, and then use any of its available methods (inherited from `Uncgits\GrouperApi\GrouperApiInterface`) to begin a new transaction.

## Advanced Usage

### Using the Container

In `ServiceProvider.php` this package binds an instance of `GrouperApiInterface` to the IoC container using the `Grouper` key. This is bound by default as a singleton, but if you prefer to change this behavior, you can do so in the `config/grouper-api.php` file.

If using the container, you can use `$myInstance = app()->make('Grouper')` or `$myInstance = resolve('Grouper')` to pull in the container instance of `GrouperApiInterface`, or rely on dependency injection:

```php
use Uncgits\GrouperApiLaravel\GrouperApiInterface as Grouper;

...

public function index(Grouper $grouper) {
    $tree = $grouper->getMemberTree("my:group:name:with:stem");
}
```

### Using the Facade

Alternatively, you can rely on the 'Grouper' Facade:

```php
use Uncgits\GrouperApiLaravel\Facades\Grouper;

...

$tree = Grouper::getMemberTree("my:group:name:with:stem");
```

> Note: remember that Laravel **always uses a singleton** when utilizing the facade. Even if you set the configuration differently, you will always receive a singleton when using this Facade.

## Configuration options

### Container binding

As covered above, you may set `use_singleton` to `false` in the config file if you prefer to get a new instance each time you resolve from the container. This setting is always ignored when using the Facade, as Facades always work with singletons.

### Notification Mode

There are two notification options built into this library. You may flash the information on the screen as a flash message, or log it in the Laravel logs (whatever you've got set).

In the `config/grouper-api.php` file, `notification_mode` is an array. Inside, add one or both of `flash` and `log`.

If you choose to roll this into a higher-level custom notification / logging system for your app, you can leave this array empty to disable both of these methods, and then handle your own logging based on the return array from the API call (the API class uses a "call stack", which you can directly access from the object or Facade. This contains each API call made in the current transaction set, including metadata and response information).

> As of version 0.2, no flash messaging package is included. You are free to install your own - recommend `laracasts/flash` or something that implements a similar API.

### Debug Mode

_Requires DebugBar by Barryvdh (`barryvdh\laravel-debugbar`)_

If you turn on Debug Mode in the `config/grouper-api.php` file, extra information about the call will be written to the Debugbar. This includes timing, hashed cache key, and full results from each call.

 > Note: in accordance with best practices for this package, this will only work if the app environment is not set to `production`


## Caching

> NOTE: Caching is not available yet. Please set `GROUPER_API_CACHING=off` in your `.env` file to avoid unexpected errors. The below documentation remains for posterity.

For speed, adherence to rate limits, and overall performance, caching is built into this library. This utilizes the Laravel Cache mechanism (which, like Logs, can be set however you want in the Laravel config). You can optionally disable this by adding `GROUPER_API_CACHING=off` in your `.env` file.

### Cache TTL

The TTL of the cache is set to 10 minutes by default, but by adding `GROUPER_API_CACHE_MINUTES=x` to your `.env` file, you can set the cache to expire after x minutes.

### Cached endpoints

Caching is performed only for specific endpoints (e.g. requests that would equate to typical `GET` requests). Obviously, you would not want to cache requests that are designed to actually modify information in Grouper (equivalent to `POST` or `PATCH` or `DELETE` requests).

By default, there is a basic set of endpoints set up in `config/grouper-api.php`. If you would like change which endpoints are / are not cached (or add your own), simply modify the `cached_endpoints` array in the config file.

---

# Version History

## 0.6.0

- Open Source BSD license
- Bump PHP library dependency to ^0.7

## 0.5

- Bump PHP library dependency to ^0.6

## 0.4

- Bump PHP library dependency to ^0.5

## 0.3.1

- Explicit requirement of library version ^0.4

## 0.3

- Fixes config keys for HTTP proxy

> NOTE: if upgrading, make sure you add the new config keys into your `grouper-api.php` config file, and set them in your `.env` file if using a proxy.

## 0.2

- Remove dependency on `standaniels/flash` - optional to install a flash package

## 0.1.1

- Support for Laravel 5.8 cache TTL value change to seconds

## 0.1

- initial release.
- provides configuration options and configuration of the base `GrouperApiInterface` class.
- Known issues:
    - Caching does not work yet.
